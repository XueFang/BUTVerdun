<?php

namespace ButDataBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="ButDataBaseBundle\Repository\UsersRepository")
 */
class Users
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usersFirstname", type="string", length=255, nullable=true)
     */
    private $usersFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="usersLastname", type="string", length=255, nullable=true)
     */
    private $usersLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="usersLogin", type="string", length=10, nullable=true)
     */
    private $usersLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="usersPassword", type="string", length=255, nullable=true)
     */
    private $usersPassword;

    /**
     * @var int
     *
     * @ORM\Column(name="usersRole", type="integer")
     */
    private $usersRole;

    /**
     * @var string
     *
     * @ORM\Column(name="usersDisplay", type="boolean", options={"default" : 0})
     */
    private $usersDisplay;

    /**
     * @var string
     *
     * @ORM\Column(name="usersJob", type="string", length=255)
     */
    private $usersJob;

    /**
     * @var string
     *
     * @ORM\Column(name="usersPhoto", type="string", length=255, nullable=true)
     */
    private $usersPhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="usersSpeech", type="text", nullable=true)
     */
    private $usersSpeech;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usersLastEdit", type="datetime")
     */
    private $usersLastEdit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usersLastConnection", type="datetime")
     */
    private $usersLastConnection;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usersFirstname
     *
     * @param string $usersFirstname
     *
     * @return Users
     */
    public function setUsersFirstname( $usersFirstname )
    {
        $this->usersFirstname = $usersFirstname;

        return $this;
    }

    /**
     * Get usersFirstname
     *
     * @return string
     */
    public function getUsersFirstname()
    {
        return $this->usersFirstname;
    }

    /**
     * Set usersLastname
     *
     * @param string $usersLastname
     *
     * @return Users
     */
    public function setUsersLastname( $usersLastname )
    {
        $this->usersLastname = $usersLastname;

        return $this;
    }

    /**
     * Get usersLastname
     *
     * @return string
     */
    public function getUsersLastname()
    {
        return $this->usersLastname;
    }

    /**
     * Set usersRole
     *
     * @param integer $usersRole
     *
     * @return Users
     */
    public function setUsersRole( $usersRole )
    {
        $this->usersRole = $usersRole;

        return $this;
    }

    /**
     * Get usersRole
     *
     * @return int
     */
    public function getUsersRole()
    {
        return $this->usersRole;
    }

    /**
     * Set usersJob
     *
     * @param string $usersJob
     *
     * @return Users
     */
    public function setUsersJob( $usersJob )
    {
        $this->usersJob = $usersJob;

        return $this;
    }

    /**
     * Get usersJob
     *
     * @return string
     */
    public function getUsersJob()
    {
        return $this->usersJob;
    }

    /**
     * Set usersPhoto
     *
     * @param string $usersPhoto
     *
     * @return Users
     */
    public function setUsersPhoto( $usersPhoto )
    {
        $this->usersPhoto = $usersPhoto;

        return $this;
    }

    /**
     * Get usersPhoto
     *
     * @return string
     */
    public function getUsersPhoto()
    {
        return $this->usersPhoto;
    }

    /**
     * Set usersSpeech
     *
     * @param string $usersSpeech
     *
     * @return Users
     */
    public function setUsersSpeech( $usersSpeech )
    {
        $this->usersSpeech = $usersSpeech;

        return $this;
    }

    /**
     * Get usersSpeech
     *
     * @return string
     */
    public function getUsersSpeech()
    {
        return $this->usersSpeech;
    }

    /**
     * Set usersLastEdit
     *
     * @param \DateTime $usersLastEdit
     *
     * @return Users
     */
    public function setUsersLastEdit( $usersLastEdit )
    {
        $this->usersLastEdit = $usersLastEdit;

        return $this;
    }

    /**
     * Get usersLastEdit
     *
     * @return \DateTime
     */
    public function getUsersLastEdit()
    {
        return $this->usersLastEdit;
    }

    /**
     * Set usersLastConnection
     *
     * @param \DateTime $usersLastConnection
     *
     * @return Users
     */
    public function setUsersLastConnection( $usersLastConnection )
    {
        $this->usersLastConnection = $usersLastConnection;

        return $this;
    }

    /**
     * Get usersLastConnection
     *
     * @return \DateTime
     */
    public function getUsersLastConnection()
    {
        return $this->usersLastConnection;
    }

    /**
     * Set usersLogin
     *
     * @param string $usersLogin
     *
     * @return Users
     */
    public function setUsersLogin( $usersLogin )
    {
        $this->usersLogin = $usersLogin;

        return $this;
    }

    /**
     * Get usersLogin
     *
     * @return string
     */
    public function getUsersLogin()
    {
        return $this->usersLogin;
    }

    /**
     * Set usersPassword
     *
     * @param string $usersPassword
     *
     * @return Users
     */
    public function setUsersPassword( $usersPassword )
    {
        $this->usersPassword = $usersPassword;

        return $this;
    }

    /**
     * Get usersPassword
     *
     * @return string
     */
    public function getUsersPassword()
    {
        return $this->usersPassword;
    }

    /**
     * Set usersDisplay
     *
     * @param boolean $usersDisplay
     *
     * @return Users
     */
    public function setUsersDisplay( $usersDisplay )
    {
        $this->usersDisplay = $usersDisplay;

        return $this;
    }

    /**
     * Get usersDisplay
     *
     * @return boolean
     */
    public function getUsersDisplay()
    {
        return $this->usersDisplay;
    }

}

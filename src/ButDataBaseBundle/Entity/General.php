<?php

namespace ButDataBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * General
 *
 * @ORM\Table(name="General")
 * @ORM\Entity(repositoryClass="ButDataBaseBundle\Repository\GeneralRepository")
 */
class General
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteTITLE", type="string", length=255)
     */
    private $websiteTITLE;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteKEYWORDS", type="text")
     */
    private $websiteKEYWORDS;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteDESCRIPTION", type="string", length=255)
     */
    private $websiteDESCRIPTION;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteFAVICON", type="string", length=255)
     */
    private $websiteFAVICON;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteENCODING", type="string", length=100)
     */
    private $websiteENCODING;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set websiteTITLE
     *
     * @param string $websiteTITLE
     *
     * @return General
     */
    public function setWebsiteTitle( $websiteTITLE )
    {
        $this->websiteTITLE = $websiteTITLE;

        return $this;
    }

    /**
     * Get websiteTITLE
     *
     * @return string
     */
    public function getWebsiteTitle()
    {
        return $this->websiteTITLE;
    }

    /**
     * Set websiteKEYWORDS
     *
     * @param string $websiteKEYWORDS
     *
     * @return General
     */
    public function setWebsiteKEYWORDS( $websiteKEYWORDS )
    {
        $this->websiteKEYWORDS = $websiteKEYWORDS;

        return $this;
    }

    /**
     * Get websiteKEYWORDS
     *
     * @return string
     */
    public function getWebsiteKEYWORDS()
    {
        return $this->websiteKEYWORDS;
    }

    /**
     * Set websiteDESCRIPTION
     *
     * @param string $websiteDESCRIPTION
     *
     * @return General
     */
    public function setWebsiteDESCRIPTION( $websiteDESCRIPTION )
    {
        $this->websiteDESCRIPTION = $websiteDESCRIPTION;

        return $this;
    }

    /**
     * Get websiteDESCRIPTION
     *
     * @return string
     */
    public function getWebsiteDESCRIPTION()
    {
        return $this->websiteDESCRIPTION;
    }

    /**
     * Set websiteFAVICON
     *
     * @param string $websiteFAVICON
     *
     * @return General
     */
    public function setWebsiteFAVICON( $websiteFAVICON )
    {
        $this->websiteFAVICON = $websiteFAVICON;

        return $this;
    }

    /**
     * Get websiteFAVICON
     *
     * @return string
     */
    public function getWebsiteFAVICON()
    {
        return $this->websiteFAVICON;
    }

    /**
     * Set websiteENCODING
     *
     * @param string $websiteENCODING
     *
     * @return General
     */
    public function setWebsiteENCODING( $websiteENCODING )
    {
        $this->websiteENCODING = $websiteENCODING;

        return $this;
    }

    /**
     * Get websiteENCODING
     *
     * @return string
     */
    public function getWebsiteENCODING()
    {
        return $this->websiteENCODING;
    }

}

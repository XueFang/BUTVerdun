<?php

namespace ButDataBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FrontUI
 *
 * @ORM\Table(name="front_u_i")
 * @ORM\Entity(repositoryClass="ButDataBaseBundle\Repository\FrontUIRepository")
 */
class FrontUI
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="headerTitle", type="string", length=255, unique=true)
     */
    private $headerTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="logoBrand", type="string", length=255, unique=true)
     */
    private $logoBrand;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=15, unique=true)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="shopAddress", type="string", length=255, unique=true)
     */
    private $shopAddress;

    /**
     * @var int
     *
     * @ORM\Column(name="shopZipCode", type="integer", length=5, unique=true)
     */
    private $shopZipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="shopCity", type="string", length=255, unique=true)
     */
    private $shopCity;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set headerTitle
     *
     * @param string $headerTitle
     *
     * @return FrontUI
     */
    public function setHeaderTitle( $headerTitle )
    {
        $this->headerTitle = $headerTitle;

        return $this;
    }

    /**
     * Get headerTitle
     *
     * @return string
     */
    public function getHeaderTitle()
    {
        return $this->headerTitle;
    }

    /**
     * Set logoBrand
     *
     * @param string $logoBrand
     *
     * @return FrontUI
     */
    public function setLogoBrand( $logoBrand )
    {
        $this->logoBrand = $logoBrand;

        return $this;
    }

    /**
     * Get logoBrand
     *
     * @return string
     */
    public function getLogoBrand()
    {
        return $this->logoBrand;
    }


    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return FrontUI
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set shopAddress
     *
     * @param string $shopAddress
     *
     * @return FrontUI
     */
    public function setShopAddress($shopAddress)
    {
        $this->shopAddress = $shopAddress;

        return $this;
    }

    /**
     * Get shopAddress
     *
     * @return string
     */
    public function getShopAddress()
    {
        return $this->shopAddress;
    }

    /**
     * Set shopZipCode
     *
     * @param integer $shopZipCode
     *
     * @return FrontUI
     */
    public function setShopZipCode($shopZipCode)
    {
        $this->shopZipCode = $shopZipCode;

        return $this;
    }

    /**
     * Get shopZipCode
     *
     * @return integer
     */
    public function getShopZipCode()
    {
        return $this->shopZipCode;
    }

    /**
     * Set shopCity
     *
     * @param string $shopCity
     *
     * @return FrontUI
     */
    public function setShopCity($shopCity)
    {
        $this->shopCity = $shopCity;

        return $this;
    }

    /**
     * Get shopCity
     *
     * @return string
     */
    public function getShopCity()
    {
        return $this->shopCity;
    }
}

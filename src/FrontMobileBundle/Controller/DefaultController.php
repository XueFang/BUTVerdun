<?php

namespace FrontMobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function indexAction()
    {
        //Récupère le service qui gère la BDD But
        $serviceButBDD = $this->container->get( 'butdatabase.datasservice' );
        return $this->render( 'FrontMobileBundle:Default:index.html.twig', array("general" => $serviceButBDD->getGeneralDatas(), "frontUI" => $serviceButBDD->getFrontUIDatas(), "users" => $serviceButBDD->getAllUserSToDisplayDatas()) );
    }

}
